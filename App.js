/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Navigator} from 'react-native';
import MyFlatList from './MyFlatList';
import FlatListBooks from './FlatListBooks';
import FlatListUsers from './FlatListUsers';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class App extends Component {
  render() {
    const routes = [
      {title: 'First Scene', index: 0},
      {title: 'Second Scene', index: 1},
    ];
    return (
      <View style={styles.container}>
        {/* <MyFlatList/> */}
        {/* <Navigator
        initialRoute={{title: 'Awesome Scene', index: 0}}
        renderScene={(route, navigator) => <Text>Hello {route.title}!</Text>}
        style={{padding: 100}}
        /> */}
      <FlatListBooks/>
      {/* <FlatListUsers/> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  navbar:{
    flexBasis: 500,
    backgroundColor: 'green',
    padding:20
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
