import React, { Component } from 'react'
import {FlatList,Text,StyleSheet,View,List} from 'react-native'

export default class FlatListUsers extends Component {
    constructor(){
        super()
        this.state={
            loading : false,
            data : [],
            page: 1,
            seed: 1,
            error: null,
            refreshing : false
        }
    }
    componentWillMount(){
       this.makeRemoteRequest()
       console.log('======',this.state)
    }    
    makeRemoteRequest=()=>{
        const {page,seed} = this.state
        const url = `https://randomuser.me/api/?page=${page}&seed=${seed}&results=30`;
        this.setState({loading:true})
        fetch(url)
        .then(res=>res.json())
        .then(res => this.setState({
            data : [...this.state.data, ...res.results],
            error: res.error || null,
            loading: false,
            refreshing: false
        }))
        .catch(err=>console.log(err))
    }
  render() {
    return (       
       <Text>Hellow</Text>
    )
  }
}

const styles = StyleSheet.create({
    txt : {
        backgroundColor : "white",
        padding: 25,
        borderWidth: 0.5,
        borderColor : '#eee',
        flexBasis: 500
    },
    box:{
        flex : 1,
        flexDirection: 'row',
    }
})