import React, { Component } from 'react'
import {FlatList,Text,StyleSheet,View} from 'react-native'

export default class MyFlatList extends Component {
    constructor(){
        super()
        this.state={
            articles :[],
            reload : false,
            page : 1
          }
    }
    componentWillMount(){
       this.fetchArticles()
    }
   
    fetchArticles(){
        const {page} = this.state
        const url =`http://35.240.238.182:8080/v1/api/articles?page=${page}&limit=25`;
        console.log('===url',url)
        fetch(url,{
            headers:{
                Authorization: "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
                ContentType : 'Application/json'
            },
            method :'GET'
        })
        .then(res=> res.json())
        .then(res=>this.setState({
            articles: res.DATA
        }))
        .catch(err => console.log('err',err))
    }
    
  handleLoadMore(){
      this.setState({
          page : this.state.page +1
      },()=>{
          this.fetchArticles();
      })
  }
  render() {
    return (
        <FlatList
            data={this.state.articles}
            renderItem={({item,index})=>(
            <View key={index} style={styles.box}>
                <Text style={styles.txt}>{item.TITLE} {'\n'}By {item.AUTHOR.NAME}</Text>
            </View>
            )}
            keyExtractor={(item)=>{return ""+item.ID}}
            refreshing={this.state.reload}
            onRefresh={()=>{this.setState({
                reload : true
            })}}
            onEndReached={()=>this.handleLoadMore()}
            onEndReachedThreshold={0}
        />
        
    )
  }
}

const styles = StyleSheet.create({
    txt : {
        flex:1,
        backgroundColor : "white",
        padding: 30,
        borderWidth: 0.5,
        borderColor : '#eee'
    },
    box:{
        flex : 1
    }
})