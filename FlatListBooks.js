import React, { Component } from 'react'
import {FlatList,Text,StyleSheet,View,List} from 'react-native'

export default class FlatListBooks extends Component {
    constructor(){
        super()
        this.state={
            books :[],
            reload : false,
            page : 1,
            loading: false
          }
    }
    componentWillMount(){
       this.fetchBooks()
    }
    fetchBooks(){
        const {page} =this.state
        const url='http://178.128.28.164:4773/api/v2/books?limit=10&page='+this.state.page;
        fetch(url,{
            headers:{
                Authorization: "Basic QVBJX1VTRVJOQU1FOkFQSV9QQVNTV09SRA==",
                ContentType : 'Application/json'
            },
            method :'GET'
          })
          .then(res=> res.json())
          .then(res=>this.setState({
              books: page ==1 ? res.data : [...this.state.books,...res.data] ,
              loading: true,
              reload : false
          }))
          .catch(err => console.log('err',err))
    }
    
    handleLoadMore(){
        this.setState({
            page: this.state.page +1,
        },()=>{
            this.fetchBooks()
            this.setState({
                loading: false
            })
        })
    }
    handleRefresh(){
        this.setState({
            reload : true,
        },()=>{            
            this.setState({
                page : 1,
                reload: false,
            })
            this.fetchBooks();
        })
    }
  render() {
    return (
      
            <FlatList
                data={this.state.books}
                renderItem={({item,index})=>(
                <View key={item.id} style={styles.box}>
                    <Text style={styles.txt}>{index} {item.title}</Text>
                </View>
                )}
                keyExtractor={(item,index)=>{return ""+index}}
                refreshing={this.state.reload}
                onRefresh={()=>this.handleRefresh()}
                onEndReached={()=>this.handleLoadMore()}
                onEndReachedThreshold={0.5}
            />  
       
       
    )
  }
}

const styles = StyleSheet.create({
    txt : {
        backgroundColor : "white",
        padding: 25,
        borderWidth: 0.5,
        borderColor : '#eee',
        flexBasis: 500
    },
    box:{
        flex : 1,
        flexDirection: 'row',
    }
})